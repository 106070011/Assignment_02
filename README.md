# Software Studio 2019 Spring Assignment 2


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
A shooting game themed as the guardians of the galaxy. The rocket has to fight against UFOs and alien monster.

# Basic Components Description : 
1. Complete game process : 
start menu => game view (3 levels) => game pause => game over
![](https://i.imgur.com/2YQkrwi.png)
![](https://i.imgur.com/R7yiYdm.png)
![](https://i.imgur.com/pVRFf7W.png)
![](https://i.imgur.com/aGhNgkx.png)
2. Basic rules : 
- player: 
(i) move: can move by keyboard `up/down` and `left/right` control
(ii) shoot: shoot bullet by pressing `spacebar`
(iii) life decreases when shot by enemy's bullets or collide with enemy
(iv) unique skill: when **power >= 5**, press keyboard `M` to release earthquake and **make all the displayed enemies disappear**
touch enemy’s bullets 
- Enemy : system can generate enemy and each enemy can move and attack with blue rays 
- Map : background will move through the game
3. Jucify mechanisms : 
- Level: there are 3 levels with increasing difficulties
(i) Level 1: enemies attack with bullets with a **normal speed**
(ii) Level 2: enemies attack with bullets with a **much faster speed, and faster frequency.** And bullets **automatic aiming** at the player
![](https://i.imgur.com/LCR2ycW.png)
(iii) Level 3: besides enemies' attack, a **boss** appears. It shoot **automatic aiming** bullets with very high frequency and will have to be killed after **15 times** of attack
![](https://i.imgur.com/CWjB3I2.png)

4. Animations : 
- Rocket: the flame behind the rocket is changing
![](https://i.imgur.com/EsoEO1x.png)
- Spaceship: enemies **randomly generated with 5 different colors,** and spaceships are **swinging** while moving
![](https://i.imgur.com/Jg4hqex.png)
- Big Boss: big boss walking angrily in level3 game
![](https://i.imgur.com/N9VLIkJ.png)

5. Particle Systems : 
when the rocket collide with enemies' bullets or enemies themselves, there will be **explosion effect.**
![](https://i.imgur.com/CWjB3I2.png)
7. Sound effects : 
- background music: bgm when playing the game. The volume can be **adjusted**
(i) Press keyboard key `K` to **increase** volume
(ii) Press keyboard key `J` to **decrease** volume
- shooting sound effect: sound effect when the rocket shoots
- explosion sound effect: sound effect when the rocket's bullets collide with enemies' bullets or enemies
7. UI :
- Score:`+2` when shoots an enemy's bullet; `+10` when shoots an enemy; `+100` when use the unique skill
- Lives: You have a total of 3 lives, when shot by enemy, the lives `-1`!
- Power: power`+1` when kill an enemy or a bullet. When power >=5 can release unique skill, and the power reset as 0
- Pause: Press keyboard key `P` to **pause** during the game, and click on **resume button** to resume.
![](https://i.imgur.com/pVRFf7W.png)
8. Appearance : nice theme, clear keyboard instructions, cute boss... XD

# Bonus Functions Description : 
1. **Automatic aiming bullet :** in level2 and level3
2. **Big boss :** requires extra bullets to kill the boss, and the boss's bullets has auto aiming abbility and are much faster
3. **Earthquake effect :** when released unique skill, the whole game page shakes for a short time

# The game need to load for a while after start!
