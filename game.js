(function(){
	var WIDTH = 800;
	var HEIGHT = 480;

	var _game = new Phaser.Game(WIDTH, HEIGHT, Phaser.AUTO, 'game');

	var lives = 3;
	var score = 0;

	var mainState = {
		preload : function(){
			this.game.load.image('bullet','assets/gbullet.png');
			this.game.load.image('ebullet','assets/ebullet.png');
			this.game.load.image('bgSpace','assets/sky2.jpg');
			this.game.load.image('life','assets/heart.png');
			this.game.load.image('resume', 'assets/resume.png', 340, 150);
			//this.game.load.image('bgSpace2','assets/starfield.png');
			this.game.load.spritesheet('ship','assets/Spritesheet_64x29.png',64,29,4);
			this.game.load.spritesheet("enemyship1","assets/eSpritesheet_40x30.png",40, 30, 6);
			this.game.load.spritesheet("enemyship2","assets/eSpritesheet_40x30_hue1.png",40, 30, 6);
			this.game.load.spritesheet("enemyship3","assets/eSpritesheet_40x30_hue2.png",40, 30, 6);
			this.game.load.spritesheet("enemyship4","assets/eSpritesheet_40x30_hue3.png",40, 30, 6);
			this.game.load.spritesheet("enemyship5","assets/eSpritesheet_40x30_hue4.png",40, 30, 6);
			this.game.load.spritesheet("explode", 'assets/explosion.png', 64, 60);
			this.game.load.spritesheet("magic", 'assets/circle.png', 128, 512);
			this.game.load.audio('bomb', 'assets/bomb.mp3');
			this.game.load.audio('hit', 'assets/hit.mp3');
			this.game.load.audio('bgm', 'assets/overworld.wav');
		},

		create : function(){
			this.lastBullet = 0;
			this.lastEnemy = 0;
			this.lastTick = 0;
			this.speed = 150;
			this.bg1Speed = 30;
			this.bg2Speed =40;
			this.bg3Speed =50;
			this.enemySpeed = 100;
			this.bulletSpeed = 300;
			this.e_bulletSpeed = 250;
			this.firingTimer = 0;
			this.magicCount = 0;
			this.pause = false;

			this.game.physics.startSystem(Phaser.Physics.ARCADE);

			this.bg = this.game.add.tileSprite(0,0,1067,500,'bgSpace');
			this.bg.autoScroll(-this.bg1Speed,0);

			//this.bg2 = this.game.add.tileSprite(0,0,800,601,'bgSpace2');
			//this.bg2.autoScroll(-this.bg2Speed,0);

			//this.bg3 = this.game.add.tileSprite(0,0,800,601,'bgSpace2');
			//this.bg3.autoScroll(-this.bg3Speed,0);
			this.bombSound = this.game.add.audio('bomb');
			this.hitSound = this.game.add.audio('hit');
			this.bombSound.volume = 0.5;
			this.hitSound.volume = 0.5;
			this.bgm = this.game.add.audio('bgm');
			this.bgm.play();
			this.bgm.loop = true;


			this.ship = this.game.add.sprite(10,HEIGHT/2, 'ship');
			this.ship.animations.add('move');
			this.ship.animations.play('move', 20, true);
			this.game.physics.arcade.enable(this.ship, Phaser.Physics.ARCADE);

			this.bullets = this.game.add.group();
			this.bullets.enableBody = true;
			this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.bullets.createMultiple(10,'bullet');			
    		this.bullets.setAll('outOfBoundsKill', true);
    		this.bullets.setAll('checkWorldBounds', true);   

			this.enemies = this.game.add.group();
			this.enemies.enableBody = true;
			this.enemies.physicsBodyType = Phaser.Physics.ARCADE;

			this.e_bullets = this.game.add.group();
			this.e_bullets.enableBody = true;
			this.e_bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.e_bullets.createMultiple(30,'ebullet');			
    		this.e_bullets.setAll('outOfBoundsKill', true);
			this.e_bullets.setAll('checkWorldBounds', true);  
			
			this.explosions = this.game.add.group();
			this.explosions.enableBody = true;
			this.explosions.physicsBodyType = Phaser.Physics.ARCADE;
			this.explosions.createMultiple(30,'explode');			
    		this.explosions.setAll('outOfBoundsKill', true);
			this.explosions.setAll('checkWorldBounds', true); 
			this.explosions.forEach( function(explosion) {
				explosion.animations.add('explosion');
			});

			var style = { font: "24px Arial", fill: "#FFFFFF", align: "left" };
			var style2 = { font: "12px Arial", fill: "#FFFFFF", align: "right" };
			this.scoreText = this.game.add.text(10,10,"Score : "+ score,style);
			this.life = this.game.add.sprite(10,38, 'life');
			this.livesText = this.game.add.text(50,38, " : " + lives,style);
			this.magicText = this.game.add.text(10,66,"Power : "+ this.magicCount,style);
			this.magicExplain = this.game.add.text(480,HEIGHT-20,"push 'm' to lauch magic power when power is above 5!",style2);
			//this.game.add.button(480, 10, 'pause', this.pause, this, 2, 1, 0);
		},

		update : function(){
			this.ship.body.velocity.setTo(0,0);
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) && this.ship.x > 0)
			{
				this.ship.body.velocity.x = -2*this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) && this.ship.x < (WIDTH-this.ship.width))
			{
				this.ship.body.velocity.x = this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.UP) && this.ship.y > 0)
			{
				this.ship.body.velocity.y = -this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN) && this.ship.y < (HEIGHT-this.ship.height))
			{
				this.ship.body.velocity.y = +this.speed;
			}

			var curTime = this.game.time.now;

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.K) && this.bgm.volume < 1){
				this.bgm.volume += 0.1;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.J) && this.bgm.volume > 0){
				this.bgm.volume -= 0.1;
			}

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.P)){
				this.game.paused = true;
				this.resume = this.game.add.sprite(WIDTH/2, HEIGHT/2, 'resume');
				this.resume.anchor.setTo(0.5, 0.5);
			}

			this.game.input.onDown.add(function() {
				if (this.game.paused){
					console.log("paused");
					this.game.paused = false;
					this.resume.destroy();
				}
			}, this);

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
			{
				if(curTime - this.lastBullet > 300)
				{
					this.fireBullet();
					this.lastBullet = curTime;
				}
			}

			if(curTime - this.lastEnemy > 1000)
			{
				this.generateEnemy();
				this.lastEnemy = curTime;
			}

			if(curTime - this.lastTick > 10000)
			{
				if(this.speed < 500)
				{
					this.speed *= 1.1;
					this.enemySpeed *= 1.1;
					this.bulletSpeed *= 1.1;
					this.bg.autoScroll(-this.bg1Speed, 0);
					//this.bg2.autoScroll(-this.bg2Speed, 0);
					//this.bg3.autoScroll(-this.bg3Speed, 0);
					this.lastTick = curTime;
				}
			}

			if (curTime - this.firingTimer > 2000)
        	{
				this.enemyFires();
				this.firingTimer = curTime;
			}

			if(this.magicCount >= 5){
				if(this.game.input.keyboard.isDown(Phaser.Keyboard.M))
				{
					this.magic();
					console.log("magic!");
					this.magicCount = 0;
					this.magicText.setText("Power : "+ this.magicCount);
				}
			}

			this.game.physics.arcade.collide(this.enemies, this.ship, this.enemyHitPlayer,null, this);
			this.game.physics.arcade.collide(this.enemies, this.bullets, this.enemyHitBullet,null, this);
			this.game.physics.arcade.collide(this.e_bullets, this.bullets, this.e_BulletHitBullet,null, this);
			this.game.physics.arcade.collide(this.e_bullets, this.ship, this.e_BulletHitPlayer,null, this);
		},

		magic : function(){
			
			this.enemies.forEach( function(enemy) {
				//this.enemies.remove(enemy);
				/*var explosion = this.explosions.getFirstExists(false);
				explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
				explosion.play('explosion', 30, false, true);*/
				enemy.kill();
			});

			this.game.camera.shake(0.02, 300);

			score += 100;
			this.scoreText.setText("Score : "+ score);
		},

		fireBullet : function(curTime){
			var bullet = this.bullets.getFirstExists(false);
			if(bullet)
			{
				bullet.reset(this.ship.x+this.ship.width,this.ship.y+this.ship.height/2);
				bullet.body.velocity.x = this.bulletSpeed;
				this.hitSound.play();
			}
		},

		enemyFires : function(curTime) {
			this.enemies.forEachAlive(function(enemy) {
				var shooter = enemy;
				//  Grab the first bullet we can from the pool
				enemyBullet = this.e_bullets.getFirstExists(false);
				if(enemyBullet){
					enemyBullet.reset(shooter.body.x, shooter.body.y);
				}
				else{
					enemyBullet = this.e_bullets.create(shooter.body.x, shooter.body.y);
				}
				enemyBullet.body.velocity.x = -this.e_bulletSpeed;
				//this.game.physics.arcade.moveToObject(enemyBullet,this.player,120);
			}, this);
		},

		generateEnemy : function(){
			var enemy = this.enemies.getFirstExists(false);
			if(enemy)
			{
				enemy.reset(WIDTH - 30,Math.floor(Math.random()*(HEIGHT-30)),'enemyship'+(1+Math.floor(Math.random()*5)));
			}
			else
			{
				enemy = this.enemies.create(WIDTH - 30,Math.floor(Math.random()*(HEIGHT-30)),'enemyship'+(1+Math.floor(Math.random()*5)));
			}
			enemy.body.velocity.x = -this.enemySpeed;
			enemy.outOfBoundsKill = true;
			enemy.checkWorldBounds = true;
			enemy.animations.add('move');
			enemy.animations.play('move',20,true);
		},

		enemyHitPlayer : function(player, enemy){
			if(this.enemies.getIndex(enemy) > -1)
				this.enemies.remove(enemy);
			enemy.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		e_BulletHitPlayer : function(player, bullet){
			if(this.e_bullets.getIndex(bullet) > -1)
				this.e_bullets.remove(bullet);
			bullet.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		enemyHitBullet : function(bullet, enemy){
			if(this.enemies.getIndex(enemy) > -1) 
				this.enemies.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 10;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		},

		e_BulletHitBullet : function(bullet, enemy){
			if(this.e_bullets.getIndex(enemy) > -1)
				this.e_bullets.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 2;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		}
	}

	var menuState = {
		preload : function(){
			this.game.load.image('bgSpace','assets/sky2.jpg');
			this.game.load.image('level1','assets/level1.png');
			this.game.load.image('level2','assets/level2.png');
			this.game.load.image('level3','assets/level3.png');
			this.game.load.image('keyP','assets/keyP.jpg');
			this.game.load.image('keyM','assets/keyM.jpg');
			this.game.load.image('keyJ','assets/keyJ.jpg');
			this.game.load.image('keyK','assets/keyK.jpg');
		},

		create : function(){
			this.speed = 10;

			this.bg = this.game.add.tileSprite(0,0,1782,600,'bgSpace');
			this.bg.autoScroll(-this.speed,0);

			var style = { font: "48px Arial", fill: "#FFFFFF", align: "center" };
			this.title = this.game.add.text(140,100,"Guardians of the Galaxy",style);

			var style2 = { font: "28px Arial", fill: "#FFFFFF", align: "center" };
			this.help = this.game.add.text(250,160,"Press Enter Key to start",style2);
			var style3 = { font: "12px Arial", fill: "#FFFFFF", align: "left" };
			
			this.game.add.button(this.game.world.centerX-200, 200, 'level1', this.level1, this, 2, 1, 0);
			this.game.add.button(this.game.world.centerX-60, 200, 'level2', this.level2, this, 2, 1, 0);
			this.game.add.button(this.game.world.centerX+80, 200, 'level3', this.level3, this, 2, 1, 0);
			this.keyP = this.game.add.sprite(200,350, 'keyP');
			this.help = this.game.add.text(240,360,"PAUSE",style3);
			this.keyM = this.game.add.sprite(300,350, 'keyM');
			this.help = this.game.add.text(340,360,"MAGIC POWER",style3);
			this.keyJ = this.game.add.sprite(450,350, 'keyJ');
			this.keyK = this.game.add.sprite(490,350, 'keyK');
			this.help = this.game.add.text(530,360,"VOLUME UP/DOWN",style3);
		},

		update : function(){
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER))
				this.game.state.start('main');
		},

		level1 : function(){
			score = 0;
			lives = 3;
			this.game.state.start('main');
			//window.location.href = "level2.js";
		},

		level2 : function(){
			score = 0;
			lives = 3;
			this.game.state.start('level2');
			//window.location.href = "level2.js";
		},

		level3 : function(){
			score = 0;
			lives = 3;
			this.game.state.start('level3');
			//window.location.href = "level2.js";
		}

	}

	var overState = {
		preload : function(){
			this.game.load.image('bgSpace','assets/sky2.jpg');
		},

		create : function(){
			this.speed = 10;

			this.bg = this.game.add.tileSprite(0,0,1782,600,'bgSpace');
			this.bg.autoScroll(-this.speed,0);

			var style = { font: "48px Arial", fill: "#FFFFFF", align: "center" };
			this.title = this.game.add.text(140,170,"Guardians of the Galaxy",style);

			var style2 = { font: "28px Arial", fill: "#FFFFFF", align: "center" };
			this.help = this.game.add.text(350,230,"Score: " + score ,style2);

			var style2 = { font: "28px Arial", fill: "#FFFFFF", align: "center" };
			this.help = this.game.add.text(180,270,"Press Enter Key to return to MENU!",style2);
		},

		update : function(){
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.ENTER)){
				lives = 3;
				score = 0;
				this.game.state.start('menu');
			}
		}
	}

	var level2State = {
		preload : function(){
			this.game.load.image('bullet','assets/gbullet.png');
			this.game.load.image('ebullet','assets/ebullet.png');
			this.game.load.image('bgSpace','assets/sky2.jpg');
			this.game.load.image('life','assets/heart.png');
			this.game.load.image('resume', 'assets/resume.png', 340, 150);
			//this.game.load.image('bgSpace2','assets/starfield.png');
			this.game.load.spritesheet('ship','assets/Spritesheet_64x29.png',64,29,4);
			this.game.load.spritesheet("enemyship1","assets/eSpritesheet_40x30.png",40, 30, 6);
			this.game.load.spritesheet("enemyship2","assets/eSpritesheet_40x30_hue1.png",40, 30, 6);
			this.game.load.spritesheet("enemyship3","assets/eSpritesheet_40x30_hue2.png",40, 30, 6);
			this.game.load.spritesheet("enemyship4","assets/eSpritesheet_40x30_hue3.png",40, 30, 6);
			this.game.load.spritesheet("enemyship5","assets/eSpritesheet_40x30_hue4.png",40, 30, 6);
			this.game.load.spritesheet("explode", 'assets/explosion.png', 64, 60);
			this.game.load.spritesheet("magic", 'assets/circle.png', 128, 512);
			this.game.load.audio('bomb', 'assets/bomb.mp3');
			this.game.load.audio('hit', 'assets/hit.mp3');
			this.game.load.audio('bgm', 'assets/overworld.wav');
		},

		create : function(){
			this.lastBullet = 0;
			this.lastEnemy = 0;
			this.lastTick = 0;
			this.speed = 150;
			this.bg1Speed = 30;
			this.bg2Speed =40;
			this.bg3Speed =50;
			this.enemySpeed = 300;
			this.bulletSpeed = 300;
			this.e_bulletSpeed = 500;
			this.firingTimer = 0;
			this.magicCount = 0;
			//this.pause = false;

			this.game.physics.startSystem(Phaser.Physics.ARCADE);

			this.bg = this.game.add.tileSprite(0,0,1067,500,'bgSpace');
			this.bg.autoScroll(-this.bg1Speed,0);

			//this.bg2 = this.game.add.tileSprite(0,0,800,601,'bgSpace2');
			//this.bg2.autoScroll(-this.bg2Speed,0);

			//this.bg3 = this.game.add.tileSprite(0,0,800,601,'bgSpace2');
			//this.bg3.autoScroll(-this.bg3Speed,0);
			this.bombSound = this.game.add.audio('bomb');
			this.hitSound = this.game.add.audio('hit');
			this.bombSound.volume = 0.5;
			this.hitSound.volume = 0.5;
			this.bgm = this.game.add.audio('bgm');
			this.bgm.play();
			this.bgm.loop = true;


			this.ship = this.game.add.sprite(10,HEIGHT/2, 'ship');
			this.ship.animations.add('move');
			this.ship.animations.play('move', 20, true);
			this.game.physics.arcade.enable(this.ship, Phaser.Physics.ARCADE);

			this.bullets = this.game.add.group();
			this.bullets.enableBody = true;
			this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.bullets.createMultiple(10,'bullet');			
    		this.bullets.setAll('outOfBoundsKill', true);
    		this.bullets.setAll('checkWorldBounds', true);   

			this.enemies = this.game.add.group();
			this.enemies.enableBody = true;
			this.enemies.physicsBodyType = Phaser.Physics.ARCADE;

			this.e_bullets = this.game.add.group();
			this.e_bullets.enableBody = true;
			this.e_bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.e_bullets.createMultiple(30,'ebullet');			
    		this.e_bullets.setAll('outOfBoundsKill', true);
			this.e_bullets.setAll('checkWorldBounds', true);  
			
			this.explosions = this.game.add.group();
			this.explosions.enableBody = true;
			this.explosions.physicsBodyType = Phaser.Physics.ARCADE;
			this.explosions.createMultiple(30,'explode');			
    		this.explosions.setAll('outOfBoundsKill', true);
			this.explosions.setAll('checkWorldBounds', true); 
			this.explosions.forEach( function(explosion) {
				explosion.animations.add('explosion');
			});

			var style = { font: "24px Arial", fill: "#FFFFFF", align: "left" };
			var style2 = { font: "12px Arial", fill: "#FFFFFF", align: "right" };
			this.scoreText = this.game.add.text(10,10,"Score : "+ score,style);
			this.life = this.game.add.sprite(10,38, 'life');
			this.livesText = this.game.add.text(50,38, " : " + lives,style);
			this.magicText = this.game.add.text(10,66,"Power : "+ this.magicCount,style);
			this.magicExplain = this.game.add.text(480,HEIGHT-20,"push 'm' to lauch magic power when power is above 5!",style2);
			
		},

		update : function(){
			this.ship.body.velocity.setTo(0,0);
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) && this.ship.x > 0)
			{
				this.ship.body.velocity.x = -2*this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) && this.ship.x < (WIDTH-this.ship.width))
			{
				this.ship.body.velocity.x = this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.UP) && this.ship.y > 0)
			{
				this.ship.body.velocity.y = -this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN) && this.ship.y < (HEIGHT-this.ship.height))
			{
				this.ship.body.velocity.y = +this.speed;
			}

			var curTime = this.game.time.now;

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.K) && this.bgm.volume < 1){
				this.bgm.volume += 0.1;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.J) && this.bgm.volume > 0){
				this.bgm.volume -= 0.1;
			}

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.P)){
				this.game.paused = true;
				this.resume = this.game.add.sprite(WIDTH/2, HEIGHT/2, 'resume');
				this.resume.anchor.setTo(0.5, 0.5);
			}

			this.game.input.onDown.add(function() {
				if (this.game.paused){
					console.log("paused");
					this.game.paused = false;
					this.resume.destroy();
				}
			}, this);

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
			{
				if(curTime - this.lastBullet > 300)
				{
					this.fireBullet();
					this.lastBullet = curTime;
				}
			}

			if(curTime - this.lastEnemy > 500)
			{
				this.generateEnemy();
				this.lastEnemy = curTime;
			}

			if(curTime - this.lastTick > 10000)
			{
				if(this.speed < 500)
				{
					this.speed *= 1.1;
					this.enemySpeed *= 1.1;
					this.bulletSpeed *= 1.1;
					this.bg.autoScroll(-this.bg1Speed, 0);
					//this.bg2.autoScroll(-this.bg2Speed, 0);
					//this.bg3.autoScroll(-this.bg3Speed, 0);
					this.lastTick = curTime;
				}
			}

			if (curTime - this.firingTimer > 3000)
        	{
				this.enemyFires();
				this.firingTimer = curTime;
			}

			if(this.magicCount >= 5){
				if(this.game.input.keyboard.isDown(Phaser.Keyboard.M))
				{
					this.magic();
					console.log("magic!");
					this.magicCount = 0;
					this.magicText.setText("Power : "+ this.magicCount);
				}
			}

			this.game.physics.arcade.collide(this.enemies, this.ship, this.enemyHitPlayer,null, this);
			this.game.physics.arcade.collide(this.enemies, this.bullets, this.enemyHitBullet,null, this);
			this.game.physics.arcade.collide(this.e_bullets, this.bullets, this.e_BulletHitBullet,null, this);
			this.game.physics.arcade.collide(this.e_bullets, this.ship, this.e_BulletHitPlayer,null, this);
		},

		magic : function(){
			
			this.enemies.forEach( function(enemy) {
				//this.enemies.remove(enemy);
				/*var explosion = this.explosions.getFirstExists(false);
				explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
				explosion.play('explosion', 30, false, true);*/
				enemy.kill();
			});

			this.game.camera.shake(0.02, 300);

			score += 100;
			this.scoreText.setText("Score : "+ score);
		},

		fireBullet : function(curTime){
			var bullet = this.bullets.getFirstExists(false);
			if(bullet)
			{
				bullet.reset(this.ship.x+this.ship.width,this.ship.y+this.ship.height/2);
				bullet.body.velocity.x = this.bulletSpeed;
				this.hitSound.play();
			}
		},

		enemyFires : function(curTime) {
			this.enemies.forEachAlive(function(enemy) {
				var shooter = enemy;
				//  Grab the first bullet we can from the pool
				enemyBullet = this.e_bullets.getFirstExists(false);
				if(enemyBullet){
					enemyBullet.reset(shooter.body.x, shooter.body.y);
				}
				else{
					enemyBullet = this.e_bullets.create(shooter.body.x, shooter.body.y);
				}
				//enemyBullet.body.velocity.x = -this.e_bulletSpeed;
				this.game.physics.arcade.moveToObject(enemyBullet,this.ship,120);
			}, this);
		},

		generateEnemy : function(){
			var enemy = this.enemies.getFirstExists(false);
			if(enemy)
			{
				enemy.reset(WIDTH - 30,Math.floor(Math.random()*(HEIGHT-30)),'enemyship'+(1+Math.floor(Math.random()*5)));
			}
			else
			{
				enemy = this.enemies.create(WIDTH - 30,Math.floor(Math.random()*(HEIGHT-30)),'enemyship'+(1+Math.floor(Math.random()*5)));
			}
			enemy.body.velocity.x = -this.enemySpeed;
			enemy.outOfBoundsKill = true;
			enemy.checkWorldBounds = true;
			enemy.animations.add('move');
			enemy.animations.play('move',20,true);
		},

		enemyHitPlayer : function(player, enemy){
			if(this.enemies.getIndex(enemy) > -1)
				this.enemies.remove(enemy);
			enemy.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		e_BulletHitPlayer : function(player, bullet){
			if(this.e_bullets.getIndex(bullet) > -1)
				this.e_bullets.remove(bullet);
			bullet.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		enemyHitBullet : function(bullet, enemy){
			if(this.enemies.getIndex(enemy) > -1) 
				this.enemies.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 10;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		},

		e_BulletHitBullet : function(bullet, enemy){
			if(this.e_bullets.getIndex(enemy) > -1)
				this.e_bullets.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 2;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		}
	}

	var level3State = {
		preload : function(){
			this.game.load.image('bullet','assets/gbullet.png');
			this.game.load.image('ebullet','assets/ebullet.png');
			this.game.load.image('bbullet','assets/rbullet.png');
			this.game.load.image('bgSpace','assets/sky2.jpg');
			this.game.load.image('life','assets/heart.png');
			this.game.load.image('resume', 'assets/resume.png', 340, 150);
			//this.game.load.image('bgSpace2','assets/starfield.png');
			this.game.load.spritesheet('ship','assets/Spritesheet_64x29.png',64,29,4);
			this.game.load.spritesheet('boss','assets/monster.png',128.5,167,4);
			this.game.load.spritesheet("enemyship1","assets/eSpritesheet_40x30.png",40, 30, 6);
			this.game.load.spritesheet("enemyship2","assets/eSpritesheet_40x30_hue1.png",40, 30, 6);
			this.game.load.spritesheet("enemyship3","assets/eSpritesheet_40x30_hue2.png",40, 30, 6);
			this.game.load.spritesheet("enemyship4","assets/eSpritesheet_40x30_hue3.png",40, 30, 6);
			this.game.load.spritesheet("enemyship5","assets/eSpritesheet_40x30_hue4.png",40, 30, 6);
			this.game.load.spritesheet("explode", 'assets/explosion.png', 64, 60);
			this.game.load.spritesheet("magic", 'assets/circle.png', 128, 512);
			this.game.load.audio('bomb', 'assets/bomb.mp3');
			this.game.load.audio('hit', 'assets/hit.mp3');
			this.game.load.audio('bgm', 'assets/overworld.wav');
		},

		create : function(){
			this.lastBullet = 0;
			this.lastEnemy = 0;
			this.lastBoss = 0;
			this.lastTick = 0;
			this.speed = 150;
			this.bg1Speed = 30;
			this.bg2Speed =40;
			this.bg3Speed =50;
			this.enemySpeed = 300;
			this.bulletSpeed = 300;
			this.e_bulletSpeed = 500;
			this.firingTimer = 0;
			this.b_firingTimer = 0;
			this.magicCount = 0;
			this.pause = false;
			this.bossCount = 15;

			this.game.physics.startSystem(Phaser.Physics.ARCADE);

			this.bg = this.game.add.tileSprite(0,0,1067,500,'bgSpace');
			this.bg.autoScroll(-this.bg1Speed,0);

			//this.bg2 = this.game.add.tileSprite(0,0,800,601,'bgSpace2');
			//this.bg2.autoScroll(-this.bg2Speed,0);

			//this.bg3 = this.game.add.tileSprite(0,0,800,601,'bgSpace2');
			//this.bg3.autoScroll(-this.bg3Speed,0);
			this.bombSound = this.game.add.audio('bomb');
			this.hitSound = this.game.add.audio('hit');
			this.bombSound.volume = 0.5;
			this.hitSound.volume = 0.5;
			this.bgm = this.game.add.audio('bgm');
			this.bgm.play();
			this.bgm.loop = true;


			this.ship = this.game.add.sprite(10,HEIGHT/2, 'ship');
			this.ship.animations.add('move');
			this.ship.animations.play('move', 20, true);
			this.game.physics.arcade.enable(this.ship, Phaser.Physics.ARCADE);

			this.boss = this.game.add.sprite(550,HEIGHT/2-100, 'boss');
			this.boss.animations.add('move');
			this.boss.animations.play('move', 20, true);
			this.game.physics.arcade.enable(this.boss, Phaser.Physics.ARCADE);
			/*this.boss = this.game.add.group();
			this.boss.enableBody = true;
			this.boss.physicsBodyType = Phaser.Physics.ARCADE;*/

			this.bullets = this.game.add.group();
			this.bullets.enableBody = true;
			this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.bullets.createMultiple(10,'bullet');			
    		this.bullets.setAll('outOfBoundsKill', true);
    		this.bullets.setAll('checkWorldBounds', true);   

			this.enemies = this.game.add.group();
			this.enemies.enableBody = true;
			this.enemies.physicsBodyType = Phaser.Physics.ARCADE;

			this.e_bullets = this.game.add.group();
			this.e_bullets.enableBody = true;
			this.e_bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.e_bullets.createMultiple(30,'ebullet');			
    		this.e_bullets.setAll('outOfBoundsKill', true);
			this.e_bullets.setAll('checkWorldBounds', true);  

			this.b_bullets = this.game.add.group();
			this.b_bullets.enableBody = true;
			this.b_bullets.physicsBodyType = Phaser.Physics.ARCADE;
			this.b_bullets.createMultiple(30,'bbullet');			
    		this.b_bullets.setAll('outOfBoundsKill', true);
			this.b_bullets.setAll('checkWorldBounds', true);  
			
			this.explosions = this.game.add.group();
			this.explosions.enableBody = true;
			this.explosions.physicsBodyType = Phaser.Physics.ARCADE;
			this.explosions.createMultiple(30,'explode');			
    		this.explosions.setAll('outOfBoundsKill', true);
			this.explosions.setAll('checkWorldBounds', true); 
			this.explosions.forEach( function(explosion) {
				explosion.animations.add('explosion');
			});

			var style = { font: "24px Arial", fill: "#FFFFFF", align: "left" };
			var style2 = { font: "12px Arial", fill: "#FFFFFF", align: "right" };
			this.scoreText = this.game.add.text(10,10,"Score : "+ score,style);
			this.life = this.game.add.sprite(10,38, 'life');
			this.livesText = this.game.add.text(50,38, " : " + lives,style);
			this.magicText = this.game.add.text(10,66,"Power : "+ this.magicCount,style);
			this.magicExplain = this.game.add.text(480,HEIGHT-20,"push 'm' to lauch magic power when power is above 5!",style2);
			
		},

		update : function(){
			this.ship.body.velocity.setTo(0,0);
			if(this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) && this.ship.x > 0)
			{
				this.ship.body.velocity.x = -2*this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) && this.ship.x < (WIDTH-this.ship.width))
			{
				this.ship.body.velocity.x = this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.UP) && this.ship.y > 0)
			{
				this.ship.body.velocity.y = -this.speed;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN) && this.ship.y < (HEIGHT-this.ship.height))
			{
				this.ship.body.velocity.y = +this.speed;
			}

			var curTime = this.game.time.now;

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.K) && this.bgm.volume < 1){
				this.bgm.volume += 0.1;
			}
			else if(this.game.input.keyboard.isDown(Phaser.Keyboard.J) && this.bgm.volume > 0){
				this.bgm.volume -= 0.1;
			}

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.P)){
				this.game.paused = true;
				this.resume = this.game.add.sprite(WIDTH/2, HEIGHT/2, 'resume');
				this.resume.anchor.setTo(0.5, 0.5);
			}

			this.game.input.onDown.add(function() {
				if (this.game.paused){
					console.log("paused");
					this.game.paused = false;
					this.resume.destroy();
				}
			}, this);

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
			{
				if(curTime - this.lastBullet > 300)
				{
					this.fireBullet();
					this.lastBullet = curTime;
				}
			}

			if(curTime - this.lastEnemy > 1000)
			{
				this.generateEnemy();
				this.lastEnemy = curTime;
			}

			if(curTime - this.lastTick > 10000)
			{
				if(this.speed < 500)
				{
					this.speed *= 1.1;
					this.enemySpeed *= 1.1;
					this.bulletSpeed *= 1.1;
					this.bg.autoScroll(-this.bg1Speed, 0);
					//this.bg2.autoScroll(-this.bg2Speed, 0);
					//this.bg3.autoScroll(-this.bg3Speed, 0);
					this.lastTick = curTime;
				}
			}

			if (curTime - this.firingTimer > 2000)
        	{
				this.enemyFires();
				this.firingTimer = curTime;
			}

			if (curTime - this.b_firingTimer > 500)
        	{
				this.bossFires();
				this.b_firingTimer = curTime;
			}

			if(this.magicCount >= 5){
				if(this.game.input.keyboard.isDown(Phaser.Keyboard.M))
				{
					this.magic();
					console.log("magic!");
					this.magicCount = 0;
					this.magicText.setText("Power : "+ this.magicCount);
				}
			}

			this.game.physics.arcade.collide(this.enemies, this.ship, this.enemyHitPlayer,null, this);
			this.game.physics.arcade.collide(this.enemies, this.bullets, this.enemyHitBullet,null, this);
			this.game.physics.arcade.collide(this.e_bullets, this.bullets, this.e_BulletHitBullet,null, this);
			this.game.physics.arcade.collide(this.e_bullets, this.ship, this.e_BulletHitPlayer,null, this);
			this.game.physics.arcade.collide(this.boss, this.ship, this.bossHitPlayer,null, this);
			this.game.physics.arcade.collide(this.boss, this.bullets, this.bossHitBullet, null, this);
			this.game.physics.arcade.collide(this.b_bullets, this.bullets, this.b_BulletHitBullet,null, this);
			this.game.physics.arcade.collide(this.b_bullets, this.ship, this.b_BulletHitPlayer,null, this);
		},

		magic : function(){
			
			this.enemies.forEach( function(enemy) {
				//this.enemies.remove(enemy);
				/*var explosion = this.explosions.getFirstExists(false);
				explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
				explosion.play('explosion', 30, false, true);*/
				enemy.kill();
			});

			this.game.camera.shake(0.02, 300);

			score += 100;
			this.scoreText.setText("Score : "+ score);
		},

		fireBullet : function(curTime){
			var bullet = this.bullets.getFirstExists(false);
			if(bullet)
			{
				bullet.reset(this.ship.x+this.ship.width,this.ship.y+this.ship.height/2);
				bullet.body.velocity.x = this.bulletSpeed;
				this.hitSound.play();
			}
		},

		enemyFires : function(curTime) {
			this.enemies.forEachAlive(function(enemy) {
				var shooter = enemy;
				//  Grab the first bullet we can from the pool
				enemyBullet = this.e_bullets.getFirstExists(false);
				if(enemyBullet){
					enemyBullet.reset(shooter.body.x, shooter.body.y);
				}
				else{
					enemyBullet = this.e_bullets.create(shooter.body.x, shooter.body.y);
				}
				enemyBullet.body.velocity.x = -this.e_bulletSpeed;
				//this.game.physics.arcade.moveToObject(enemyBullet,this.player,120);
			}, this);
		},

		bossFires : function(curTime) {
			console.log("boss fires");
			if(this.boss.alive){
				enemyBullet = this.b_bullets.getFirstExists(false);
				if(enemyBullet){
					enemyBullet.reset(this.boss.body.x, this.boss.body.y);
				}
				else{
					enemyBullet = this.b_bullets.create(this.boss.body.x, this.boss.body.y+50);
				}
				this.game.physics.arcade.moveToObject(enemyBullet,this.ship,120);
			}
		},

		generateEnemy : function(){
			var enemy = this.enemies.getFirstExists(false);
			if(enemy)
			{
				enemy.reset(WIDTH - 30,Math.floor(Math.random()*(HEIGHT-30)),'enemyship'+(1+Math.floor(Math.random()*5)));
			}
			else
			{
				enemy = this.enemies.create(WIDTH - 30,Math.floor(Math.random()*(HEIGHT-30)),'enemyship'+(1+Math.floor(Math.random()*5)));
			}
			enemy.body.velocity.x = -this.enemySpeed;
			enemy.outOfBoundsKill = true;
			enemy.checkWorldBounds = true;
			enemy.animations.add('move');
			enemy.animations.play('move',20,true);
		},

		enemyHitPlayer : function(player, enemy){
			if(this.enemies.getIndex(enemy) > -1)
				this.enemies.remove(enemy);
			enemy.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		bossHitPlayer : function(player, enemy){
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		e_BulletHitPlayer : function(player, bullet){
			if(this.e_bullets.getIndex(bullet) > -1)
				this.e_bullets.remove(bullet);
			bullet.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		b_BulletHitPlayer : function(player, bullet){
			if(this.b_bullets.getIndex(bullet) > -1)
				this.b_bullets.remove(bullet);
			bullet.kill();
			lives -= 1;
			this.livesText.setText(" : "+ lives);
			if(lives < 0){
				this.bgm.stop();
				this.game.state.start('over');
			}
		},

		enemyHitBullet : function(bullet, enemy){
			if(this.enemies.getIndex(enemy) > -1) 
				this.enemies.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 10;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		},

		e_BulletHitBullet : function(bullet, enemy){
			if(this.e_bullets.getIndex(enemy) > -1)
				this.e_bullets.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 2;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		},

		b_BulletHitBullet : function(bullet, enemy){
			if(this.b_bullets.getIndex(enemy) > -1)
				this.b_bullets.remove(enemy);

			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			enemy.kill();
			bullet.kill();
			score += 2;
			this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		},

		bossHitBullet : function(bullet, enemy){
			var explosion = this.explosions.getFirstExists(false);
			explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
			explosion.play('explosion', 30, false, true);
			this.bombSound.play();

			if(this.bossCount > 0){
				console.log("hit boss");
				this.bossCount -= 1;
				this.boss.body.velocity=0;
			}
			else{
				console.log("kill boss");
				bullet.kill();
				score += 50;
				this.scoreText.setText("Score : "+ score);
			}

			enemy.kill();
			//score += 10;
			//this.scoreText.setText("Score : "+ score);
			this.magicCount += 1;
			this.magicText.setText("Power : "+ this.magicCount);
		},
	}

	_game.state.add('main', mainState);
	_game.state.add('menu', menuState);
	_game.state.add('over', overState);
	//_game.state.add('resume', resumeState);
	_game.state.add('level2', level2State);
	_game.state.add('level3', level3State);
	_game.state.start('menu');
})();